Crux
====

Crux is a state container for Rust.  
It's not about application architecture but rather about readability and extensibility.  
  
I plan to use this with [Piston](http://www.piston.rs/).  

## Test

```bash
$ cargo test
```

## License

MIT
